package com.khunpon.lab8;

public class Circle {
    private String name;
    private double radian;

    public Circle(String name, int radian) {
        this.name = name;
        this.radian = radian;
    }

    public int printCircleRadian() {
        double area = 3.14 * (radian * radian);
        System.out.println(name + "Area = " + area);
        return (int) area;
    }

    public int printCirclePerimeter() {
        double perimeter = 2 * 3.14 * radian;
        System.out.println(name + "Perimeter = " + perimeter);
        return (int) perimeter;
    }
}
