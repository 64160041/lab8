package com.khunpon.lab8;

public class Rectangle {
    private String name;
    private int width;
    private int height;

    public Rectangle(String name,int width , int height) {
        this.name = name ;
        this.height = height;
        this.width = width;
    }

    public int printRectangleArea() {
        int area = height * width;
        System.out.println(name + " area = " + area);
        return area;
    }
    public int printRectanglePerimeter() {
        int perimeter = (height + width) * 2 ;
        System.out.println(name + " perimeter = " + perimeter);
        return perimeter;
    }
    
}